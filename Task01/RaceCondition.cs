using Shared.Contracts;
using Shared.Entities;

namespace Task01;

public class RaceCondition : ITask
{
    public void Run()
    {
        Console.Clear();
        PerformUnsafeDemonstration();

        Console.WriteLine(new string('#',20));

        PerformSafeDemonstration();
    }

    private void PerformUnsafeDemonstration()
    {
        var bankAccount = new BankAccount(100);
        var consumer = new OperationConsumer();

        PerformOperations(bankAccount, consumer, false);

        Console.WriteLine($"Final account balance after unsafe withdrawal: {bankAccount.Balance}");
    }

    private void PerformSafeDemonstration()
    {
        var bankAccount = new BankAccount(100);
        var consumer = new OperationConsumer();

        PerformOperations(bankAccount, consumer, true);

        Console.WriteLine($"Final account balance after safe withdrawal: {bankAccount.Balance}");
    }

    private void PerformOperations(BankAccount bankAccount, OperationConsumer consumer, bool safeMode)
    {
        var threads = Enumerable.Range(0, 5).Select(_ =>
                new Thread(() => consumer.PerformOperation(Operation.Withdraw(bankAccount, 50, safeMode))))
            .ToList();

        threads.ForEach(t => t.Start());
        threads.ForEach(t => t.Join());
    }
}