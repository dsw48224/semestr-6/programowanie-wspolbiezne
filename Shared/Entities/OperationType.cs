namespace Shared.Entities;

public enum OperationType
{
    Withdraw,
    Deposit,
    Transfer
}