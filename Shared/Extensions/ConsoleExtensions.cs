namespace Shared.Extensions;

public static class ConsoleExtensions
{
    public static int ReadInt(string message, Func<int, bool> func)
    {
        var input = "";
        while (int.TryParse(input, out var result) == false && !func(result))
        {
            Console.Write(message);
            input = Console.ReadLine();
        }

        return int.Parse(input!);
    }
}