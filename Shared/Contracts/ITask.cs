namespace Shared.Contracts;

public interface ITask
{
    public void Run();
}