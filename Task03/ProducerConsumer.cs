using Shared.Contracts;
using Shared.Entities;
using Shared.Extensions;
using Buffer = Shared.Entities.Buffer;

namespace Task03;

public class ProducerConsumer : ITask
{
    public void Run()
    {
        Console.Clear();

        var bufferSize = ConsoleExtensions.ReadInt("Input buffer size: ", x => x > 0);
        var sharedBuffer = new Buffer(bufferSize);

        var bankAccountsSize = ConsoleExtensions.ReadInt("Input bank accounts size: ", x => x > 1);
        var bankAccounts = Enumerable.Range(1, bankAccountsSize).Select(x => new BankAccount(x * 100)).ToArray();

        var producerThreadsCount = ConsoleExtensions.ReadInt("Input producer threads count: ", x => x > 0);
        var consumerThreadsCount = ConsoleExtensions.ReadInt("Input consumer threads count: ", x => x > 0);

        PerformOperations(sharedBuffer, bankAccounts, producerThreadsCount, consumerThreadsCount);
    }

    private void PerformOperations(Buffer sharedBuffer, BankAccount[] bankAccounts, int producerThreadsCount, int consumerThreadsCount)
    {
        var threads = new List<Thread>();

        threads.AddRange(Enumerable.Range(0, producerThreadsCount).Select(_ => new Thread(() =>
        {
            var producer = new OperationProducer(sharedBuffer, bankAccounts);
            producer.StartProducing();
        })));

        threads.AddRange(Enumerable.Range(0, consumerThreadsCount).Select(_ => new Thread(() =>
        {
            var consumer = new OperationConsumer(sharedBuffer);
            consumer.StartConsuming();
        })));

        threads.ForEach(t => t.Start());
        threads.ForEach(t => t.Join());
    }
}