﻿using Shared.Contracts;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        var tasks = new Dictionary<char, ITask>
        {
            { '1', new Task01.RaceCondition() },
            { '2', new Task02.Deadlock() },
            { '3', new Task03.ProducerConsumer() }
        };

        while (true)
        {
            DisplayMenu();

            var input = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (input == 'q')
                break;

            if (tasks.TryGetValue(input, out var selectedTask))
            {
                selectedTask.Run();
            }
            else
            {
                Console.WriteLine("Invalid input");
            }

            WaitForKey();
        }
    }

    static void DisplayMenu()
    {
        Console.Clear();
        Console.WriteLine("DSW - Programowanie Współbieżne");
        Console.WriteLine("Jan Kliszcz, Indeks: 48224");
        Console.WriteLine(new string('#', 20));
        Console.WriteLine("Select task: ");
        Console.WriteLine("\t1 - Race condition example");
        Console.WriteLine("\t2 - Deadlock example");
        Console.WriteLine("\t3 - Producer/Consumer example");
        Console.WriteLine("q - Exit");
        Console.Write("Input:");
    }

    static void WaitForKey()
    {
        Console.Write("Press any key to continue ...");
        Console.ReadKey();
    }
}