using Shared.Contracts;
using Shared.Entities;

namespace Task02;

public class Deadlock : ITask
{
    public void Run()
    {
        var runMode = GetRunMode();

        var bankAccountA = new BankAccount(100);
        var bankAccountB = new BankAccount(200);

        var consumer = new OperationConsumer();

        PerformOperations(consumer, bankAccountA, bankAccountB, runMode);
    }

    private char GetRunMode()
    {
        Console.WriteLine("Select run mode:");
        Console.WriteLine("\ts - safe mode");
        Console.WriteLine("\tu - unsafe mode (program blocks)");

        var input = ' ';
        while (input != 's' && input != 'u')
        {
            Console.Write("Input:");
            input = Console.ReadKey().KeyChar;
            Console.WriteLine();
        }

        return input;
    }

    private void PerformOperations(OperationConsumer consumer, BankAccount bankAccountA, BankAccount bankAccountB, char runMode)
    {
        var threads = Enumerable.Range(0, 2).Select(_ => new Thread(() =>
        {
            consumer.PerformOperation(_ == 0
                ? Operation.Transfer(bankAccountA, bankAccountB, 100, runMode == 's')
                : Operation.Transfer(bankAccountB, bankAccountA, 50, runMode == 's'));
        })).ToList();

        threads.ForEach(t => t.Start());
        threads.ForEach(t => t.Join());
    }
}